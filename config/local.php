<?php
return [
    //'autoload' => [
    //    '/home/jerry.lundegaard/Projects/some-vivo-project'
    //],
    'objects' => [
	'db' => [
		'class' => 'Vivo\DB\MySQL',
		'arguments' => [
		    'dsn' => 'host=localhost;dbname=vivo-cms;charset=utf8',
		    'user' => 'developer'
		]
    	]
    ],
    'debug' => false,
    'profile' => false,
];