#!/bin/sh

echo "1/4 composer download"
php -r "readfile('https://getcomposer.org/installer');" | php

echo "2/4 project installation"
php composer.phar install

# (optional step for developers) set VIVO_HOME environment variable to use local copy of Vivo adding 
# SetEnv VIVO_HOME /path/to/vivo
# to public/.htaccess or virtual host  

echo "3/4 symlink to meta site"
cd data/repository
ln -s ../../vendor/lundegaard/vivo/data/repository/META-SITE
# or 
#ln -s $VIVO_HOME/data/repository/META-SITE

echo "4/4 edit config/local.php now"



