<?php
namespace SampleProject\Event;

use Vivo\Object;
use Vivo\CMS\Event\EventHandlerInterface;
use Vivo\CMS\Http\Controller;

class MyRootViewEventHandler extends Object implements EventHandlerInterface
{
    /**
     * @var Controller
     */
    private $controller;
    
    public function __construct(Controller $controller) 
    {
        $this->controller = $controller;
    }
    
    public function onEvent($type, $subject) 
    {
        //echo "<!-- event $type (controller: $this->controller)-->";
    }
}
