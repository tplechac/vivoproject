{assign var=root_path value="ROOT/"}
{assign var=root_document value=$request->site->getEntity($root_path)}
<div id="main">
	<div id="main-wrapper">
		<div id="header">
			{* home link *}
			{entityLink entity=$root_document attributes="title=title class=logo" content='<img src="/modules/vivo/images/default/logo.png" title="Vivo CMS site - HOMEPAGE" alt="logo: Vivo CMS site - HOMEPAGE" />'}
			{* /home link *}
			<span class="claim">Web Building Platform and Content Management System</span>
		</div>
		<div id="content">
			<div id="content-holder">


				{* navigation path *}
				{if $request->document->parent->getDepth() > 0}
				<div id="breadcrumb">
					<ul>
						{foreach from=$request->document->getParents('Vivo\CMS\Model\Document') item=document}
						{assign var=title value=$document->title|escape}
							<li>{entityLink entity=$document attributes="title=$title"}<span class="slash"></span></li>
						{/foreach}
						<li>{$request->document->title|escape}</li>
					</ul>
					<div class="clear"></div>
				</div>
				{/if}
				{* /navigation path *}


				{* context menu *}
				{if $request->document->parent->getDepth() > 0}
					<div class="floatLeft" id="aside">
						<ul>
							{foreach from=$request->document->parent->getAvailableChildren() item=document1}
								<li>
									{if $document1 eq $request->document}
										{entityLink entity=$document1}
										
										{* submenu (optional) *}
										{assign var=children1 value=$document1->getAvailableChildren()}
										{if $children1}
											<ul>
												{foreach from=$children1 item=document2}
													<li>
														{entityLink entity=$document2}
													</li>
												{/foreach}
											</ul>
										{/if}
										{* /submenu *}
					
									{else}
										{entityLink entity=$document1}
									{/if}
					
								</li>
							{/foreach}
						</ul>
					</div>
					{* /context menu *}
					<div class="floatRight" id="contentPanel">
				{/if}
					<h1>{$request->document->getHeading()}</h1>
					{$this->param->view()}					
				{if $request->document->parent->getDepth() > 0}
					</div>
				{/if}
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>
		<div id="haddons">			
			{* menu (2 levels) *}
			<ul id="nav">
				{foreach from=$root_document->getAvailableChildren() item=document1}
					<li {if $request->document->under($document1)} class="active" {/if}>
						{entityLink entity=$document1}
						{assign var=children1 value=$document1->getAvailableChildren()}
						{if $children1}
							<ul>
								{foreach from=$document1->getAvailableChildren() item=document2}
									<li>
										{entityLink entity=$document2}
									</li>
								{/foreach}
							</ul>
						{/if}
					</li>
				{/foreach}
			</ul>
			{* /menu *}
					
			<div id="search-box">
				<form action="/vyhledavani/" method="get">
					<fieldset>
						<legend>Hledání</legend>
						<label for="search-field">Hledat: </label>
						<input type="hidden" value="main-&gt;param-&gt;param-&gt;doSearch" name="act" />
						<input type="text" name="query" id="search-field" class="inputSearch" />
						<input type="submit" class="btnSearch" value="Hledat" />
					</fieldset>
				</form>	
			</div>						
			<div id="lang-box">
				<ul>
					<li class="active"><a href="/cs/">CS</a></li>
					<li class=""><a href="/en/">EN</a></li>
					<li class=""><a href="/de/">DE</a></li>
				</ul>
			</div>
		</div>		
		<div class="clear"></div>
	</div>
</div>
<div id="footer">
	<div id="footer-holder">		
		<ul class="nav">									
			<li><a href="#">Odkaz #1</a></li>
			<li><a href="#">Odkaz #2</a></li>
			<li><a href="#">Odkaz #3</a></li>
			<li><a href="#">Odkaz #4</a></li>
			<li><a href="#">Odkaz #5</a></li>
			<li><a title="Mapa stránek" href="/cs/mapa-stranek/">Mapa stránek</a></li>
		</ul>	
		<div id="lundeLinks" class="floatRight">
				<a target="_blank" class="vivo" title="Powered by CMS LARS Vivo - open in new window" href="http://vivo.lundegaard.cz/">Powered by CMS LARS Vivo</a>
		</div>
		<div  class="clear"></div>
		<p>
			&copy; {$smarty.now|date_format:"%Y"} <a href="http://www.lundegaard.cz/" target="_blank" title="Lundegaard - e-business solutions provider - open in new window">Lundegaard spol. s r.o.</a><br />
			ISO 9001:2008
		</p>
		<div class="clear"></div>
	</div>			
</div>
